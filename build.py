import os
from buildtools import os_utils
YARN = os_utils.assertWhich('yarn')
NODE = os_utils.assertWhich('node')
GIT = os_utils.assertWhich('git')
os_utils.ensureDirExists('packed')
with os_utils.Chdir('highlightjs-src'):
    os_utils.cmd([GIT, 'reset', '--hard'], echo=True, show_output=True, critical=True)
    os_utils.cmd([YARN], echo=True, show_output=True, critical=True)
    os_utils.cmd([NODE, 'tools/build.js', '-t', 'browser', ':common'], echo=True, show_output=True, critical=True)
os_utils.single_copy(os.path.join('highlightjs-src','build','highlight.min.js'), os.path.join('packed'), verbose=True)
os_utils.single_copy(os.path.join('highlightjs-src','LICENSE'), os.path.join('packed'), verbose=True)
os_utils.single_copy(os.path.join('highlightjs-src','CHANGES.md'), os.path.join('packed'), verbose=True)
os_utils.single_copy(os.path.join('highlightjs-src','README.md'), os.path.join('packed'), verbose=True)
os_utils.single_copy(os.path.join('highlightjs-src','README.ru.md'), os.path.join('packed'), verbose=True)
os_utils.copytree(os.path.join('highlightjs-src','src','styles'), os.path.join('packed', 'styles'), verbose=True)
