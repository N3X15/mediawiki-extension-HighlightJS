import json, jsonschema, requests, yaml

PADDING=15 # len(extension.json)+1
def check_json_file(filename, schema=None):
    print(filename, end=' '*(PADDING - len(filename)))
    i = None
    with open(filename, 'r') as f:
        i = json.load(f)
    if schema:
        with open(schema, 'r') as f:
            s = json.load(f)
        jsonschema.validate(i,s)
    print('OK')

def check_yaml_file(filename, schema=None):
    print(filename, end=' '*(PADDING - len(filename)))
    i = None
    with open(filename, 'r') as f:
        i = yaml.load(f)
    if schema:
        with open('dev/extension.schema.v2.json', 'r') as f:
            s = json.load(f)
        jsonschema.validate(i,s)
    print('OK')

check_json_file('extension.json') # schema='dev/extension.schema.v2.json'
check_yaml_file('mwi.yml')
